package main

import (
	"fmt"
	"io"
	"os"
)

func main(){
	file, err := os.Create("hello.bin")
	if err != nil{
		fmt.Println("Ошибка создания файла")
		os.Exit(1)
	}

	DataWrite := []byte("Hello Devs! \nIts my new project")
	file.Write(DataWrite)
	file, err = os.Open("hello.bin")
	if err != nil{
		fmt.Println(err)
		os.Exit(1)
	}


	DataRead := make([]byte, 64)

	for{
		n, err := file.Read(DataRead)
		if err == io.EOF{
		}
		fmt.Print(string(DataRead[:n]))
	}

}
